<?php
session_name("login");
session_start();

if ($_SESSION['autorizado']==true)
{

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Inventario CC</title>

<script language="javascript" src="JS/jquery-1.4.2.min.js" type="text/javascript"></script>

<script language="javascript" src="JS/jquery.corners.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="CSS/principal.css" />
<link rel="stylesheet" type="text/css" href="CSS/formulario.css" />
<script language="javascript" src="JS/funciones3.js" type="text/javascript"></script>
</head>

<body>

<div id="contenedor">

	<div id="cabecera">
	<input class="caja" name="nivel" id="nivel" type="text" value="<?php echo $_SESSION['nivel'] ;?>" hidden="hidden" disabled="disabled" size="25" maxlength="45" />
   
    
    </div>
    <div id="menu">
    <ul>
    <li ><a href="#"  onclick="llamarRegistros();">Registros</a></li>
   <li ><a href="#"  onclick="llamarReportes();">Reportes</a></li>
    </ul>
    </div>
    
    <div id="sesion" align="right">
	<?php
	if ($_SESSION['autorizado']==true)
{

echo "usuario: ".$_SESSION['usuario']."   |";
?>

<a href="PHP/cerrarSesion.php">Cerrar Sesion</a>
	<?php } ?>
    	
    </div>
    
    <div id="izquierdo">
	
    
    </div>
	<div id="derecho">
    
    </div>
    
    <div id="cuerpo">
    	<div id="resultado">
        <center>
       	
</center>
        </div>
    </div>
    
    
    
    <div id="pie">
    </div>

</div>

</body>
</html>

<?php
}
else
{
	header("location:index.html");
}

?>