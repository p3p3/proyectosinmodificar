<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


<meta name="google-site-verification" content="mrp4cZXPgeCTjEY5fbYkV4PasAYkgla8i_l_uR4nEVY" />	
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" type="text/css" href="/assets/fa4904c6/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/assets/fa4904c6/css/bootstrap-responsive.min.css" />
<link rel="stylesheet" type="text/css" href="/assets/fa4904c6/css/bootstrap-yii.css" />
<link rel="stylesheet" type="text/css" href="/assets/fa4904c6/css/jquery-ui-bootstrap.css" />
<link rel="stylesheet" type="text/css" href="/css/pager.css" />
<script type="text/javascript" src="/assets/7e42a305/jquery.min.js"></script>
<script type="text/javascript" src="/assets/fa4904c6/js/bootstrap.bootbox.min.js"></script>
<script type="text/javascript" src="/assets/fa4904c6/js/bootstrap.min.js"></script>
<title>ITVillahermosa</title>


<link rel="icon" type="image/png" href="/images/favicon.ico" />
<link rel="apple-touch-icon" href="/themes/plantilla/images/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="/themes/plantilla/images/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="/themes/plantilla/images/apple-touch-icon-114x114.png">

<!--[if lt IE 9]>
	<script src="/themes/plantilla/scripts/html5.js"></script>
<![endif]-->

<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="/themes/plantilla/stylesheets/style.css">
<link rel="stylesheet" href="/themes/plantilla/stylesheets/responsive.css"> 
<link rel="stylesheet" href="/themes/plantilla/stylesheets/jquery.onebyone.css">
<link rel="stylesheet" href="/themes/plantilla/stylesheets/prettyPhoto.css">

<!--<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">-->
<!--<link rel="stylesheet" type="text/css" href="/css/bootstrap-responsive.min.css">-->

<!--<script src="/scripts/jquery.min.js"></script>--> 
<!--<script src="/js/bootstrap.js"></script>-->
<!--<script src="/js/bootstrap.min.js"></script>-->

<script src="/themes/plantilla/scripts/jquery.prettyPhoto.js"></script>
<script src="/themes/plantilla/scripts/jquery.blackandwhite.min.js"></script>
<script src="/themes/plantilla/scripts/jquery.bxSlider.min.js"></script>
<script src="/themes/plantilla/scripts/jquery.faq.js"></script> 
<script src="/themes/plantilla/scripts/jquery.simpleFAQ-0.7.min.js"></script>  
<script src="/themes/plantilla/scripts/jquery.onebyone.min.js"></script>              
<script src="/themes/plantilla/scripts/jquery.touchwipe.min.js"></script> 
<script src="/themes/plantilla/scripts/js_func.js"></script>

<!--<script src="/js/bootstrap-collapse.js"></script>-->




<script>
$(function(){
    $('#obo_slider').oneByOne({
		className: 'oneByOne1',	             
		easeType: 'random',
		slideShow: true
	});  

})
$(function(){
	$(".gallery a[rel^='prettyPhoto']").prettyPhoto({theme: 'dark_rounded'});
    
})

$(function(){
	/*$('.clients_slider').bxSlider({
		auto: false,
		controls : false,
		mode: 'fade',
		pager: true
	});*/	
	/*$('.recent_slider').bxSlider({
		auto: false,
		displaySlideQty: 1,
		moveSlideQty: 1,
    	speed : 1000
	});
	*/
	$('#faq').dltoggle();
	$("#open").click(function(event){
      $('#faq').dltoggle_show();
      return false;
	});
	$("#close").click(function(event){
      $('#faq').dltoggle_hide();
      return false;
	});
   
})

</script>
</head>
<body>

<!--start header-->


<section id="header">
<!-- Include the header bar -->
    <div class="wraper"> <!--abre envoltura -->
 
 <header class="encabezado">
 
	<div class="imagen1">
  		<img src="/images/logosep.jpg" alt="SEP"/>
	</div>
       
  	<div class="titulo1">
    <p>Instituto Tecnológico de Villahermosa</p>
    </div>
    <div class="titulo2">
    <p>"Tierra, Tiempo, Trabajo y Tecnología"</p>
    </div>  
    
  </header>
</div> <!--cierra envoltura  del encabezado --><!-- /.container -->  
</section><!-- /#header --><!-- Require the header -->
<!-- Require the navigation -->
<div class="wraper"><!--inicia envoltura -->
	<section id="navigation-main">
    	<div class="navbar">
        	<div class="navbar-inner">
            	<a data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar">
                	<span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                
                <div class="nav-collapse" style="font-family:SoberanaTitularBold; font-size:0.96em">
                	<ul class="nav">
                    	<li><!--<a href="http://www.itvillahermosa.edu.mx"><i class="icon-home"></i></a>-->
                        <a href="/site.jsp"><i class="icon-home"></i></a>                        </li>
                        
                        <li class="dropdown">
                        	<a data-toggle="dropdown" class="dropdown-toggle" href="#">Identidad <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                            	<li><a href="/site/identidad.jsp?view=historia">Historia</a></li>
                                <li><a href="/site/identidad.jsp?view=filosofia">Filosofia</a></li>
                                <li><a href="/site/identidad.jsp?view=mensaje">Mensaje del Director</a></li>
                                <li><a href="/site/identidad.jsp?view=directoriotel">Directorio Telefonico</a></li>
                                <li><a href="/site/identidad.jsp?view=directorioper">Directorio Personal</a></li>
                            </ul>
                        </li>
                        
                        <li class="dropdown">
                        	<a data-toggle="dropdown" class="dropdown-toggle" href="#">Oferta Educativa <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                            	<li class="dropdown">
                                	<a class="dropdown-toggle" href="/site/oferta.jsp?view=Presencialescolarizado">Presencial Escolarizado<b class="caret"></b></a>                                	<ul class="dropdown-menu sub-menu ">
                                    	<li><a href="/site/oferta.jsp?view=Ingenieriacivil">Ing. Civil</a></li>
                                        <li><a href="/site/oferta.jsp?view=Ingenieriaquimica">Ing. Química</a></li>
                                        <li><a href="/site/oferta.jsp?view=Ingenieriaambiental">Ing. Ambiental</a></li>
                                        <li><a href="/site/oferta.jsp?view=Ingenieriaindustrial">Ing. Industrial</a></li>
                                        <li><a href="/site/oferta.jsp?view=Ingenieriabioquimica">Ing. Bioquímica</a></li>
                                        <li><a href="/site/oferta.jsp?view=Licenciaturaadministracion">Lic. en Administración</a></li>
                                        <li><a href="/site/oferta.jsp?view=Ingenieriagestionempresarial">Ing. en Gestión Empresarial</a> </li>
                                        <li><a href="/site/oferta.jsp?view=Ingenieriaensistemas">Ing. en Sistemas Computacionales</a> </li>
                                        <li><a href="/site/oferta.jsp?view=Ingenieriatic">Ing. en Tecnologías de la Información y Comunicaciones</a></li>
                                    </ul>
                                </li>    
                                <li class="dropdown">
                                	<a class="dropdown-toggle" href="/site/oferta.jsp?view=Ingenieriaensistemasvirtual">Virtual&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b class="caret"></b></a>                                	<ul class="dropdown-menu sub-menu ">
                                    	<li><a href="/site/oferta.jsp?view=Ingenieriaensistemasvirtual">Ing. en Sistemas Computacionales</a> </li>
  						<li><a href="/site/oferta.jsp?view=Ingenieriaindustrial">Ing. Industrial</a></li>
                                    </ul>
                                </li>    
                                                                
                                <li class="dropdown">
                                	<a class="dropdown-toggle" href="/site/oferta.jsp?view=Posgrado">Posgrado&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b class="caret"></b></a>                                	<ul class="dropdown-menu sub-menu ">
                                    	<li> </li>
                                         <li><a href="/site/posgrado.jsp?view=index">Maestría En Planeación De Empresas y Desarrollo Regional</a> </li>
                                        <li><a href="/site/oferta.jsp?view=Maestriaentecnologias">Maestría en Tecnologías de la Información</a> </li>                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                        	<a data-toggle="dropdown" class="dropdown-toggle" href="#">Calidad <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                            	<li><a href="/site/calidad.jsp?view=compoli">Compromisos y Políticas</a></li>
                                <li><a href="/site/calidad.jsp?view=sisambiental">Sistema De Gestión Ambiental</a></li>
                                <li><a href="/site/calidad.jsp?view=siscalidad">Sistema De Gestión De Calidad</a></li>
                                <li><a href="/site/calidad.jsp?view=sisequidad">Sistema De Gestión De Equidad De Genero</a></li>
                            </ul>
                        </li>
                        
                        <li class="dropdown">
                        	<a data-toggle="dropdown" class="dropdown-toggle" href="#">Centro de Servicios<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                            	
                                <li class="dropdown">
                                	<a class="dropdown-toggle" href="/site/sys.jsp?view=alumnos">Alumnos&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b class="caret"></b></a>                                    <ul class="dropdown-menu sub-menu ">
                                    	<li><a href="/site/difusion.jsp?view=calendario">Calendario </a></li>
                                        <li><a target="_blank" href="http://cc.itvillahermosa.edu.mx/sys/sie.php">SIE Alumnos</a></li>
                                        <li><a target="_blank" href="http://cc.itvillahermosa.edu.mx/sys/desaca/evaldoc2">Evaluación Docente Ene-Jun 2014</a></li>
                                        <li><a target="_blank" href="http://cc.itvillahermosa.edu.mx/sys/recfin/sysref">Referencias Bancarias</a></li>
                                        <li><a target="_blank" href="http://cc.itvillahermosa.edu.mx/sys/estpro/srp">Residencias Profesionales</a></li>
                                        <li><a href="/site/sys.jsp?view=alumnos">Centro de Servicios</a></li>
                                    </ul>
                                </li> 
                                <li class="divider"></li>
                                <li class="dropdown">
                                	<a class="dropdown-toggle" href="/site/sys.jsp?view=alumnos">Egresados&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b class="caret"></b></a>                                    <ul class="dropdown-menu sub-menu ">
                                    	<li><a target="_blank" href="http://cc.itvillahermosa.edu.mx/sys/estpro/scott2">SCOTT</a></li>
                                        <li><a target="_blank" href="http://cc.itvillahermosa.edu.mx/sys/recfin/sysref">Referencias Bancarias</a></li>
                                        <li><a target="_blank" href="http://cc.itvillahermosa.edu.mx/sys/estpro/scott2/pdflib/examenes.php?hoy=2&amp;origen=web">Exámenes de Titulación</a></li>
                                        <li><a href="/site/sys.jsp?view=alumnos">Centro de Servicios</a></li>
                                    </ul>
                                </li> 
                                <li class="divider"></li>
                                <li class="dropdown">
                                	<a class="dropdown-toggle" href="/site/sys.jsp?view=Docentes">Docentes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b class="caret"></b></a>                                	<ul class="dropdown-menu sub-menu ">
                                    	<li><a target="_blank" href="http://cc.itvillahermosa.edu.mx/sys/estpro/scott2">SCOTT</a></li>
                                    	<li><a target="_blank" href="http://cc.itvillahermosa.edu.mx/sys/sied.php">SIE Docentes</a></li>
                                        <li><a href="/site/sys.jsp?view=Docentes">Centro de Servicios</a></li>
                                    </ul>
                                </li> 
                                <li class="divider"></li>
                                <li><a target="_blank" href="http://mail.google.com/a/itvillahermosa.edu.mx">Correo Institutional</a></li>
				    <li class="divider"></li>
                                <li><a target="_blank" href="http://portalautoservicios.sep.gob.mx/login.jsp">Descarga del Talon de Pago</a></li>
                                <li class="divider"></li>
                                <li><a href="/site/sys.jsp?view=acceso">Acceso Intranet</a></li>
                            </ul>
                        </li>
                        
                        <li class="dropdown">
                        	<a data-toggle="dropdown" class="dropdown-toggle" href="#">Difusión <b class="caret"></b></a>
                            <ul class="dropdown-menu">
				    <li><a href="/site/difusion.jsp?view=Convocatorias">Convocatorias</a></li>
                                <li><a href="/site/difusion.jsp?view=aviso">Avisos</a></li>
                                <li><a href="/site/difusion.jsp?view=logotipo">Logotipos</a></li>
                                <li><a href="/site/difusion.jsp?view=calendario">Calendario Escolar</a></li>
                                <li><a href="/site/calidad.jsp?view=normalinea">Normas y Lineamientos</a></li>
                                <li><a href="/site/difusion.jsp?view=procedimientos">Procedimientos</a></li>
                                <li><a href="/site/login.jsp">login</a></li>
                            </ul>
                        </li>
                        
                        <li class="dropdown">
                        	<a data-toggle="dropdown" class="dropdown-toggle" href="#">Departamentos <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                            	<li><a href="/site/departamentos.jsp?view=SubAcademico">Académicos</a></li>
                                <li><a href="/site/departamentos.jsp?view=SubPlaneacion">Planeación Y Vinculación</a></li>
                                <li><a href="/site/departamentos.jsp?view=SubAdministrativa">Servicios Administrativos</a></li>
                            </ul>
                        </li>
                    </ul>
                </div><!-- /.nav-collapse -->
            </div><!-- /#div nav bar inner -->
        </div><!-- /#div nav bar -->
    </section><!-- /#navigation-main -->
</div>  <!-- cierra la envoltura del menu -->



<!-- Include content pages -->
<section class="main-body">

    <div class="container">
        <div id="content">
            <div class="content_block">
    <div class="wraper">
        <div class="wrape homeone">
           
            <div class="fallback"><center><img src="logoitvhpie2.png" alt="ITVH" style="max-width:30%;" /></center><br/><br/></div>
		 

            <div id="obo_slider">
		<div class="oneByOne_item">
                    <img src='/images/slider/carrera.png'  class='wp1_3 wp1_left slide2_bot' style='max-width:440px' alt=''/>                    <span class="txt1 txt_right2">28 de Septiembre</span> 
                    <span class="txt2 txt_right2 green">Carrera Atlética</span>
		    <span class="txt3 txt_right2"><h3><b>Participa!</b></h3></span>
                    <span class="txt4 txt_right2"><a class="btn_l" target="_blank" href="http://carreraatletica.itvillahermosa.edu.mx">Ver</a></span>               
                </div>
		
		<div class="oneByOne_item">
                	<a href='http://www.itvillahermosa.edu.mx/convocatoria/viewConvocatoria/43.jsp' target='_blank'><img src='/images/slider/becas_SEP_superior.jpg'  class='wp1_3 wp1_left slide2_bot'/></a>   
         	</div>

		
		<div class="oneByOne_item">
                    <img src='/images/slider/aniversario40logo.jpg'  class='wp1_3 wp1_left slide2_bot' style='max-width:450px' alt=''/>                    <span class="txt1 txt_right2">Actividades del mes de septiembre</span> 
                    <span class="txt2 txt_right2 green">40 ANIVERSARIO</span>
		    <span class="txt3 txt_right2"><h3><b>Participa!</b></h3></span>
                    <span class="txt4 txt_right2"><a href="/aviso/viewAviso/35.jsp" target="_self" class="btn_l">Ver Ahora..</a></span>                
                </div>
	
		<div class="oneByOne_item">
                    <img src='/images/slider/calendario2.fw.png'  class='wp1_3 wp1_left slide2_bot' style='max-width:450px' alt=''/>                    <span class="txt1 txt_right2">Periodo Agosto - Diciembre 2014</span> 
                    <span class="txt2 txt_right2 green">CALENDARIO ESCOLAR</span>
                    <span class="txt4 txt_right2"><a href="/convocatoria/viewConvocatoria/38.jsp" target="_self" class="btn_l">Ver Ahora..</a></span>                
                </div>
			 <div class="oneByOne_item">
                	<img src='/images/slider/lap1.jpg'  class='wp1_3 wp1_left slide2_bot' style='max-width:400px' alt=''/>                     <span class="txt1 txt_right2"> </span>
                    <span class="txt2 txt_right2 green">ESTUDIA UNA CARRERA EN MODALIDAD VIRTUAL<br/></span>
                    <span class="txt4 txt_right2"><a class="btn_l" target="_blank" href="http://itvillahermosa.edu.mx/aviso/viewAviso/31.jsp">Ver</a></span>
                </div>
		<div class="oneByOne_item">
                    <img src='/images/slider/maestrias.png'  class='wp1_3 wp1_left slide2_bot' style='max-width:400px' alt=''/>                    <span class="txt1 txt_right2">Periodo Agosto - Diciembre 2014</span> 
                    <span class="txt2 txt_right2 green">Oferta Educativa<br/>de Posgrados</span>
		      <!--<span class="txt3 txt_right2"><h3><b>¡Incluyendo la Maestría en Tecnologías de la Información!</b></h3></span>-->
                    <span class="txt4 txt_right2"><a class="btn_l" target="_blank" href="http://www.itvillahermosa.edu.mx/convocatoria/viewConvocatoria/35.jsp">Ver Ahora..</a></span>
                </div>
						 
		  <div class="oneByOne_item">
                    <img src='/images/slider/ciide2t.fw.png'  class='wp1_3 wp1_left slide2_bot' style='max-width:400px' alt=''/>                    <span class="txt1 txt_right2">&nbsp;conferencia magistral</span> 
                    <span class="txt2 txt_right2 green">“La formación de ingenieros que demandan México<br /> y el mundo” </span>
					     <span class="txt4 txt_right2"><a class="btn_l" target="_blank" href="/site/video.jsp?view=conferenciamagistral">Ver Ahora..</a></span>                
                </div>
                <div class="oneByOne_item">
                	<img src='/images/slider/banner3.jpg'  class='wp1_3 wp1_left slide2_bot' style='max-width:300px' alt=''/>                     <span class="txt1 txt_right2"> </span>
                    <span class="txt2 txt_right2 green">Impacto y perspectiva como política educativa</span>
                    <span class="txt4 txt_right2"><a class="btn_l" href="http://www.itvillahermosa.edu.mx/convocatoria/viewConvocatoria/33.jsp">Ver Convocatoria.</a></span>
                </div>
                <div class="oneByOne_item">
                    <img src='/images/slider/Familia.jpg'  class='wp1_3 wp1_left slide2_bot' style='max-width:300px' alt=''/>                    <span class="txt1 txt_right2">&nbsp;</span> 
                    <span class="txt2 txt_right2 green">Planificación <br />Familiar</span>
					     <span class="txt4 txt_right2"><a class="btn_l" target="_blank" href="/site/video.jsp?view=planificacionfamiliar">Ver Ahora..</a></span>                
                </div>
		

		
		<!--<div class="oneByOne_item">
                    <img src='/images/slider/evaluaciondocente.png'  class='wp1_3 wp1_left slide2_bot' style='max-width:450px' alt=''/>                    <span class="txt1 txt_right2">¡Atención Alumnos!</span> 
                    <span class="txt2 txt_right2 green">Evaluación Docente</span>
                    <span class="txt3 txt_right2"><b>Periodo Enero - Junio 2014</b></span>
                    <span class="txt4 txt_right2 txt4up"><a href="http://cc.itvillahermosa.edu.mx/sys/desaca/evaldoc2" target="_blank" class="btn_l">¡Ya Disponible!</a></span>
                </div>-->
                <!--                 <div class="oneByOne_item">
                    <img src='/images/slider/nuevoingresoene2014.png'  class='wp1_3 wp1_left slide2_bot' style='max-width:450px' alt=''/>                    <span class="txt1 txt_right2">Periodo Agosto - Diciembre 2014</span> 
                    <span class="txt2 txt_right2 green">Convocatoria de<br/>Nuevo Ingreso</span>
                    <span class="txt4 txt_right2"><a href="/convocatoria/viewConvocatoria/14.jsp" target="_blank" class="btn_l">Ver Ahora..</a></span>                
                </div>
                  -->
                
		  <!--<div class="oneByOne_item">
                	<img src='/images/slider/LOGOAJ.fw.png'  class='wp1_3 wp1_left slide2_bot' style='max-width:400px' alt=''/>                    <span class="txt1 txt_right2">Academia Journals </span> 
                    <span class="txt2 txt_right2 green">Talleres </span>
                    <span class="txt3 txt_right2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;21, 22 y 23 de mayo 2014, Alusivo al XL Aniversario</span>
                    <span class="txt4 txt_right2"><a class="btn_l" href="http://academiajournals.itvillahermosa.edu.mx/journals/site/congreso.jsp?view=talleres">Ver Talleres.</a></span>
                </div>-->         
 
		
		
		<!--<div class="oneByOne_item">


                                        <span class="txt1 txt_right2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Instituto Tecnológico de Villahermosa</span> 
                    <span class="txt5 txt_right2 green">CampTI 2014</span>
                    <span class="txt4 txt_right2"></span>
		</div>-->


		
               <!-- <div class="oneByOne_item">
                    <img src='/images/slider/LOGOAJ.fw.png'  class='wp1_3 wp1_left slide2_bot' style='max-width:450px' alt=''/>                    <span class="txt1 txt_right2">Academia Journals</span> 
                    <span class="txt2 txt_right2 green">Congreso internacional de investigación</span>
                    <span class="txt3 txt_right2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;21, 22 y 23 de mayo 2014, Alusivo al XL Aniversario</span>
                    <span class="txt4 txt_right2"><a class="btn_l" target="_blank" href="http://academiajournals.itvillahermosa.edu.mx/">Ver Ahora..</a></span>
                </div>-->
                
                <!--<div class="oneByOne_item">
                    <a href='http://www.modeloeducativo.sep.gob.mx' target='_blank'><img src='/images/slider/consulta.jpg'  class='wp1_3 wp1_left slide2_bot'/></a>                </div>-->
                
               
                <!--<div class="oneByOne_item">
                                        <span class="txt1 txt_right2"></span> 
                    <span class="txt2 txt_right2 green">¿QUÉ ES LA REFORMA EDUCATIVA?</span>
                    <span class="txt4 txt_right2"><a class="btn_l" target="_blank" href="http://www.presidencia.gob.mx/reformaeducativa/">Ver Ahora..</a></span>
                </div>-->

                			

            </div>
        </div>
<div class="content_block">
 <div class="wraper">
 
  <!-- features_list -->
   
  <div  class="wraper">
                 <div class="send_quote">
             <h4>Resultados de Admisión<br />y Horarios de Integración</h4>
             <p><b>¡Agosto-Diciembre 2014!</b>
             <a class="btn_col btn_green" href="http://itvillahermosa.edu.mx/site/sys.jsp?view=nuevoingreso"><b>Ver Resultados</b></a></p>
             </div> 
			<!-Resultados de examen de admision -->
             	<!--<hr/>-->
  <div class="featured_list">
   <div class="features_slider_wrap">
    <ul class="features_slider gallery">
     <li><div><div class="bwWrapper">
     <a href="/site/difusion.jsp?view=Convocatorias"><img src="/themes/plantilla/images/portfolio/convocatorias.jpg" width="300" height="190" alt="" class="iefix" /></a>   
     <p><strong> Convocatorias</strong></p></div></div>
     </li>
    <li><div><div class="bwWrapper">
     <a href="/site/difusion.jsp?view=aviso"><img src="/themes/plantilla/images/portfolio/avisos.jpg" width="300" height="190" alt="" class="iefix" /></a>   
     <p><strong> Avisos</strong></p></div></div>
     </li>
      <li><div><div class="bwWrapper">
     <a href="/site/difusion.jsp?view=procedimientos"><img src="/themes/plantilla/images/portfolio/procesos.jpg" width="300" height="190" alt="" class="iefix" /></a>   
     <p><strong>Procedimientos 2014 </strong></p></div></div>
     </li>
     <li><div><div class="bwWrapper">
     <a href="/site/sys.jsp?view=alumnos"><img src="/themes/plantilla/images/portfolio/centrodeservicio.jpg" width="300" height="190" alt="" class="iefix" /></a>   
     <p><strong> Centro de Servicios</strong></p></div></div>
     
    
   </ul>
   </div>
      <hr />
  </div>
  </div>

  
  <!-- /features_list -->
 </div>
</div> 

 <div class="wraper">
  <!-- blog entries -->
  <div class="blog_entries1">
           
       <div class="post post_medium">
                   
          <div class="">
            <div class="blog_entries1">
<div class="post post_medium">
    <div style="max-width:300px;" class="img_wrap">
        
        <div class="date">23 de septiembre de 2014</div>
        
     <a href="/nota/viewNota/56.jsp"><img src="/uploads/PLAN_COM_DIF/2014/NOTAS/files/VERBENA%20TEC/0.JPG" width="300" height="190" alt="" class="iefix" /></a>    </div>
    <div class="desc">
        <h4> <a style="color:#1D7044;" href="/nota/viewNota/56.jsp">LA COMUNIDAD ESCOLAR DEL ITVH CELEBRAN EN GRANDE EL 40 ANIVERSARIO DE ...</a></h4>
        <p>  La cultura y el deporte fueron también otros importantes motivos para festejar en grande el 40 Aniversario del Instituto Tecnológico Villahermosa; esto se pudo ver reflejado en el festival artístico cultural que se realizó el miércoles 17 de septiembre en la plaza “La Ceiba” a partir de las...</p>
                   <blockquote style="border-left:initial;font-style: italic;"><img src="/themes/plantilla/images/avatar.png" alt="" />Por COMUNICACION Y DIFUSION</blockquote><br />
           <div class="metadata">
                   
            <a class="boton button green normal more-link" href="/nota/viewNota/56.jsp">Leer Mas</a>        </div>  
    </div>
   
</div>
</div>
 <div class="blog_entries1">
<div class="post post_medium">
    <div style="max-width:300px;" class="img_wrap">
        
        <div class="date">18 de septiembre de 2014</div>
        
     <a href="/nota/viewNota/55.jsp"><img src="/uploads/PLAN_COM_DIF/2014/NOTAS/files/DESFILE%2016-09-14/0.JPG" width="300" height="190" alt="" class="iefix" /></a>    </div>
    <div class="desc">
        <h4> <a style="color:#1D7044;" href="/nota/viewNota/55.jsp">EXCELENTE PARTICIPACION DEL CONTIGENTE REPRESENTATIVO DEL ITVH EN EL D...</a></h4>
        <p>  El Instituto Tecnológico de Villahermosa, estuvo dignamente representado durante el desfile cívico militar conmemorativo del inicio de la Independencia de México, en su 204 Aniversario, que tuvo lugar el martes 16 de septiembre a partir de las 10:00 a.m. por las principales calles y avenidas de l...</p>
                   <blockquote style="border-left:initial;font-style: italic;"><img src="/themes/plantilla/images/avatar.png" alt="" />Por COMUNICACION Y DIFUSION</blockquote><br />
           <div class="metadata">
                   
            <a class="boton button green normal more-link" href="/nota/viewNota/55.jsp">Leer Mas</a>        </div>  
    </div>
   
</div>
</div>
 <div class="blog_entries1">
<div class="post post_medium">
    <div style="max-width:300px;" class="img_wrap">
        
        <div class="date">17 de septiembre de 2014</div>
        
     <a href="/nota/viewNota/54.jsp"><img src="/uploads/PLAN_COM_DIF/2014/NOTAS/files/SEMANA%20DE%20VINCULACI%C3%93N%20XL%20ANIVERSARIO%20ITVH/0.JPG" width="300" height="190" alt="" class="iefix" /></a>    </div>
    <div class="desc">
        <h4> <a style="color:#1D7044;" href="/nota/viewNota/54.jsp">LAS JORNADAS DE VINCULACIÓN EN EL MARCO DEL 40 ANIVERSARIO DEL ITVH S...</a></h4>
        <p>  La comunidad escolar del Instituto Tecnológico de Villahermosa festejó académicamente todo el mes de septiembre, la satisfacción y el orgullo de 40 años de servicios educativos ininterrumpidos en pro del desarrollo del Estado y la región. Es así como del 8 al 12 de septiembre se llevó a cabo...</p>
                   <blockquote style="border-left:initial;font-style: italic;"><img src="/themes/plantilla/images/avatar.png" alt="" />Por COMUNICACION Y DIFUSION</blockquote><br />
           <div class="metadata">
                   
            <a class="boton button green normal more-link" href="/nota/viewNota/54.jsp">Leer Mas</a>        </div>  
    </div>
   
</div>
</div>
 <div class="blog_entries1">
<div class="post post_medium">
    <div style="max-width:300px;" class="img_wrap">
        
        <div class="date">11 de septiembre de 2014</div>
        
     <a href="/nota/viewNota/53.jsp"><img src="/uploads/PLAN_COM_DIF/2014/NOTAS/files/CELEBRACI%C3%93N%20XL%20ANIVERSARIO/0.JPG" width="300" height="190" alt="" class="iefix" /></a>    </div>
    <div class="desc">
        <h4> <a style="color:#1D7044;" href="/nota/viewNota/53.jsp">EL ITVH DE MANTELES LARGOS. EMOTIVO FESTEJO DE ANIVERSARIO</a></h4>
        <p>  La comunidad escolar del Instituto Tecnológico de Villahermosa, celebró con mucho júbilo su 40 Aniversario, partiendo el tradicional pastel conmemorativo que se realizó en la plaza “La Ceiba” a partir de las 14:00 horas; el cual fue ofrecido con gran entusiasmo por las autoridades educativas...</p>
                   <blockquote style="border-left:initial;font-style: italic;"><img src="/themes/plantilla/images/avatar.png" alt="" />Por COMUNICACION Y DIFUSION</blockquote><br />
           <div class="metadata">
                   
            <a class="boton button green normal more-link" href="/nota/viewNota/53.jsp">Leer Mas</a>        </div>  
    </div>
   
</div>
</div>
 

                              Ir a la página:<ul id="yw0" class="yiiPager"><li class="first hidden"><a href="/site/index.jsp">Inicio</a></li>
<li class="previous hidden"><a href="/site/index.jsp"><</a></li>
<li class="page selected"><a href="/site/index.jsp">1</a></li>
<li class="page"><a href="/site/index.jsp?page=2">2</a></li>
<li class="page"><a href="/site/index.jsp?page=3">3</a></li>
<li class="page"><a href="/site/index.jsp?page=4">4</a></li>
<li class="page"><a href="/site/index.jsp?page=5">5</a></li>
<li class="next"><a href="/site/index.jsp?page=2">></a></li>
<li class="last"><a href="/site/index.jsp?page=12">Fin</a></li></ul>  

      </div>
          
      </div>
        
        <!-- convocatorias -->
        
           
<!--     <h2 align="center">Compromisos y Politicas del ITVH</h2><br/>-->
     <div class="faq_list">
     
     	<ul id="faq">
           <li class="lis">
               <p class="question" style="font-size: 1.0em;font-weight: normal;">Convocatorias<strong>   </strong></p>
              <div class="answer">

<table class="table table-hover" cellspacing=5 cellmargin=0 border=0 width="90%">
                                                                <tr>
                <td width='5%' valign='top'><img src='/themes/plantilla/images/next.png'></td>
                <td width='65%'>Convocatoria de Residencias Profesionales Ago Dic 2014</td>
                <td width='15%' valign='top'>
                <a class="btn-success btn_col btn_green" type="button" href="/convocatoria/viewConvocatoria/24.jsp">Ver mas..</a>                </td>
                 <!-- secci�n de visitados
                 <td width='10%' valign='top'>  Consultas</td> -->
           </tr>
    
    
                                                                 <tr>
                <td width='5%' valign='top'><img src='/themes/plantilla/images/next.png'></td>
                <td width='65%'>CONVOCATORIA SERVICIO SOCIAL 2014</td>
                <td width='15%' valign='top'>
                <a class="btn-success btn_col btn_green" type="button" href="/convocatoria/viewConvocatoria/29.jsp">Ver mas..</a>                </td>
                 <!-- secci�n de visitados
                 <td width='10%' valign='top'>  Consultas</td> -->
           </tr>
    
    
                                                                 <tr>
                <td width='5%' valign='top'><img src='/themes/plantilla/images/next.png'></td>
                <td width='65%'>Paquete Informativo de Residencias Profesionales Ago-Dic 2014</td>
                <td width='15%' valign='top'>
                <a class="btn-success btn_col btn_green" type="button" href="/convocatoria/viewConvocatoria/31.jsp">Ver mas..</a>                </td>
                 <!-- secci�n de visitados
                 <td width='10%' valign='top'>  Consultas</td> -->
           </tr>
    
    
                                                                 <tr>
                <td width='5%' valign='top'><img src='/themes/plantilla/images/next.png'></td>
                <td width='65%'>SEXTO ENCUENTRO NACIONAL DE TUTORÍA, IMPACTO Y PERSPECTIVA COMO POLÍTICA EDUCATIVA.</td>
                <td width='15%' valign='top'>
                <a class="btn-success btn_col btn_green" type="button" href="/convocatoria/viewConvocatoria/33.jsp">Ver mas..</a>                </td>
                 <!-- secci�n de visitados
                 <td width='10%' valign='top'>  Consultas</td> -->
           </tr>
    
    
                                                                 <tr>
                <td width='5%' valign='top'><img src='/themes/plantilla/images/next.png'></td>
                <td width='65%'>SEMANA ACADÉMICA 2014</td>
                <td width='15%' valign='top'>
                <a class="btn-success btn_col btn_green" type="button" href="/convocatoria/viewConvocatoria/41.jsp">Ver mas..</a>                </td>
                 <!-- secci�n de visitados
                 <td width='10%' valign='top'>  Consultas</td> -->
           </tr>
    
    
                                                                 <tr>
                <td width='5%' valign='top'><img src='/themes/plantilla/images/next.png'></td>
                <td width='65%'>PROGRAMA NACIONAL DE BECAS 2014</td>
                <td width='15%' valign='top'>
                <a class="btn-success btn_col btn_green" type="button" href="/convocatoria/viewConvocatoria/43.jsp">Ver mas..</a>                </td>
                 <!-- secci�n de visitados
                 <td width='10%' valign='top'>  Consultas</td> -->
           </tr>
    
    
     </table>
</div>
 
            </li>
           <li class="lis">
               <p class="question" style="font-size: 1.0em;font-weight: normal;">Calendario, Boletines, Talleres, Cursos y  Diplomados</p>
              <div class="answer">

<table class="table table-hover" cellspacing=5 cellmargin=0 border=0 width="90%">
                                                                <tr>
                <td width='5%' valign='top'><img src='/themes/plantilla/images/next.png'></td>
                <td width='65%'>Calendario Escolar</td>
                <td width='15%' valign='top'>
                <a class="btn-success btn_col btn_green" type="button" href="/convocatoria/viewConvocatoria/38.jsp">Ver mas..</a>                </td>
                 <!-- secci�n de visitados
                 <td width='10%' valign='top'>  Consultas</td> -->
           </tr>
    
    
     </table>
</div>
 
            </li>
            <li class="lis">
               <p class="question" style="font-size: 1.0em;font-weight: normal;">Avisos</p>
              <div class="answer">

<table class="table table-hover" cellspacing=5 cellmargin=0 border=0 width="90%">
                                                                <tr>
                <td width='5%' valign='top'><img src='/themes/plantilla/images/next.png'></td>
                <td width='65%'>ESTUDIANTES PRÓXIMOS A EGRESAR. GRADUACIÓN</td>
                <td width='15%' valign='top'>
                <a class="btn-success btn_col btn_green" type="button" href="/aviso/viewAviso/38.jsp">Ver mas..</a>                </td>
                 <!-- secci�n de visitados
                 <td width='10%' valign='top'>  Consultas</td> -->
           </tr>
    
    
                                                                 <tr>
                <td width='5%' valign='top'><img src='/themes/plantilla/images/next.png'></td>
                <td width='65%'>ESTUDIANTES DE NUEVO INGRESO. TOMA DE FOTOS PARA CREDENCIALES</td>
                <td width='15%' valign='top'>
                <a class="btn-success btn_col btn_green" type="button" href="/aviso/viewAviso/39.jsp">Ver mas..</a>                </td>
                 <!-- secci�n de visitados
                 <td width='10%' valign='top'>  Consultas</td> -->
           </tr>
    
    
                                                                 <tr>
                <td width='5%' valign='top'><img src='/themes/plantilla/images/next.png'></td>
                <td width='65%'>Seguro Institucional</td>
                <td width='15%' valign='top'>
                <a class="btn-success btn_col btn_green" type="button" href="/aviso/viewAviso/29.jsp">Ver mas..</a>                </td>
                 <!-- secci�n de visitados
                 <td width='10%' valign='top'>  Consultas</td> -->
           </tr>
    
    
                                                                 <tr>
                <td width='5%' valign='top'><img src='/themes/plantilla/images/next.png'></td>
                <td width='65%'>SERVICIOS ESTUDIANTILES  INFORMA A LOS TITULADOS QUE PUEDEN PASAR A RECOGER TITULOS Y CÉDULAS</td>
                <td width='15%' valign='top'>
                <a class="btn-success btn_col btn_green" type="button" href="/aviso/viewAviso/17.jsp">Ver mas..</a>                </td>
                 <!-- secci�n de visitados
                 <td width='10%' valign='top'>  Consultas</td> -->
           </tr>
    
    
                                                                 <tr>
                <td width='5%' valign='top'><img src='/themes/plantilla/images/next.png'></td>
                <td width='65%'>SERVICIOS ESTUDIANTILES INFORMA A LOS TITULADOS QUE PUEDEN PASAR A RECOGER TITULOS Y CÉDULAS</td>
                <td width='15%' valign='top'>
                <a class="btn-success btn_col btn_green" type="button" href="/aviso/viewAviso/18.jsp">Ver mas..</a>                </td>
                 <!-- secci�n de visitados
                 <td width='10%' valign='top'>  Consultas</td> -->
           </tr>
    
    
                                                                 <tr>
                <td width='5%' valign='top'><img src='/themes/plantilla/images/next.png'></td>
                <td width='65%'>SERVICIOS ESTUDIANTILES INFORMA A LOS TITULADOS QUE PUEDEN PASAR A RECOGER TITULOS Y CÉDULAS (3)</td>
                <td width='15%' valign='top'>
                <a class="btn-success btn_col btn_green" type="button" href="/aviso/viewAviso/19.jsp">Ver mas..</a>                </td>
                 <!-- secci�n de visitados
                 <td width='10%' valign='top'>  Consultas</td> -->
           </tr>
    
    
                                                                 <tr>
                <td width='5%' valign='top'><img src='/themes/plantilla/images/next.png'></td>
                <td width='65%'>SERVICIOS ESTUDIANTILES INFORMA A LOS TITULADOS QUE PUEDEN PASAR A RECOGER TITULOS Y CÉDULAS (4)</td>
                <td width='15%' valign='top'>
                <a class="btn-success btn_col btn_green" type="button" href="/aviso/viewAviso/25.jsp">Ver mas..</a>                </td>
                 <!-- secci�n de visitados
                 <td width='10%' valign='top'>  Consultas</td> -->
           </tr>
    
    
                                                                 <tr>
                <td width='5%' valign='top'><img src='/themes/plantilla/images/next.png'></td>
                <td width='65%'>SERVICIOS ESTUDIANTILES INFORMA A LOS TITULADOS EGRESADOS A CONTINUACIÓN PASEN POR TITULO Y CEDULA</td>
                <td width='15%' valign='top'>
                <a class="btn-success btn_col btn_green" type="button" href="/aviso/viewAviso/30.jsp">Ver mas..</a>                </td>
                 <!-- secci�n de visitados
                 <td width='10%' valign='top'>  Consultas</td> -->
           </tr>
    
    
     </table>
</div>
 
            </li>
        </ul>
        
     </div>
     <!-- faq-->

  </div>  <!-- /blog entries -->
  <!-- sidebar -->
  <div class="sidebar">
    <div class="social_icons">
        <ul>
         <li><a class="facebook" href="http://www.facebook.com/ITVillahermosa">Facebook</a></li>
         <li><a class="tweeter" href="http://twitter.com/itvh">Twitter</a></li>
         <li><a class="google" href="http://plus.google.com/101013671265577542882">Google+</a></li>
         <li><a class="rss" href="http://www.youtube.com/user/ITVillahermosa1"></a></li>
         <!--<li><a class="www" href="#">www</a></li>-->
        </ul>
</div>    <br/><br/><hr />
    <div>
	<form action="" class="navbar-search pull-left">
    	<input type="text" placeholder="Buscar" class="search-query spa2">
    </form>
        </div>
    <br/><br/><hr />
    
            <div style="max-width:284px;" class="bwWrapper"><a href='http://cc.itvillahermosa.edu.mx/sys/sie.php' target="_blank"><img src="/images/sie.jpg" width="284" height="128" alt="" /></a></div>
       <br/>
        <div style="max-width:284px;" class="bwWrapper"><a href='http://educacionvirtual.itvillahermosa.edu.mx' target="_blank"><img src="/images/eaula.jpg" width="284" height="128" alt="" /></a></div>
        <br/>
         <div style="max-width:284px;" class="bwWrapper"><a href='http://www.snit.mx' target="_blank"><img src="/images/dgest.jpg" width="284" height="128" alt="" /></a></div>
        <br />
        <div style="max-width:284px;" class="bwWrapper"><a href='http://www.sep.gob.mx' target="_blank"><img src="/images/sep.jpg" width="284" height="128" alt="" /></a></div>
        <br />
	  <div style="max-width:284px;" class="bwWrapper"><a href='/site/login' ><img src="/images/pnpc.jpg" width="284" height="128" alt="" /></a></div>
        <br />    
    <!-- /Botones de Accesos Rapidos -->
 <div class="recent_work">
    	<h4>Accesos rápidos:</h4>
        <ul class="recent_slider">
        	<li>
                    <div style="max-width:60px;" class="bwWrapper"><a href='http://mail.google.com/a/itvillahermosa.edu.mx' target="_blank"><img src="/themes/plantilla/images/icons/icon-mail.png" width="60" height="55" alt="" /></a></div>
                    <div style="max-width:60px;" class="bwWrapper"><a href="/site/difusion.jsp?view=calendario"><img src="/themes/plantilla/images/icons/icon-calendar.png" width="60" height="55" alt="" /></a></div>
                    <div style="max-width:60px;" class="bwWrapper"><a href="/site/sys.jsp?view=acceso"><img src="/themes/plantilla/images/icons/icon-key-hole.png" width="60" height="55" alt="" /></a></div>
        	    <div style="max-width:60px;" class="bwWrapper"><a href="/site/sys.jsp?view=alumnos"><img src="/themes/plantilla/images/icons/icon-gears.png" width="60" height="55" alt="" /></a></div>
                    <div style="max-width:60px;" class="bwWrapper"><a href="/site.jsp"><img src="/themes/plantilla/images/icons/icon-chat.png" width="60" height="55" alt="" /></a></div>
                    <div style="max-width:60px;" class="bwWrapper"><a href="/site.jsp"><img src="/themes/plantilla/images/icons/icon-write.png" width="60" height="55" alt="" /></a></div>
                </li>        
        </ul>
    </div><!-- /recent work -->

 
    
</div>
  <!-- /sidebar -->
 </div>
</div>


       
      
      
            </div><!-- content -->
	</div>

</section>

<!-- Require the footer -->
<script type="text/javascript">
    $(document).ready(function(){
  
        $(window).scroll(function(){
            if ($(this).scrollTop() > 100) {
                $('.scrollup').fadeIn();
            } else {
                $('.scrollup').fadeOut();
            }
        });
  
        $('.scrollup').click(function(){
            $("html, body").animate({ scrollTop: 0 }, 600);
            return false;
        });
  
    });
</script>

<style type="text/css">
.scrollup{
    width:60px;
    height:60px;
    opacity:0.6;
    position:fixed;
    bottom:50px;
    right:100px;
    display:none;
    text-indent:-9999px;
    background: url('/themes/plantilla/images/up.png') no-repeat;
	/*background-color: #6ac36a;*/
}
</style>

<a href="#" class="scrollup">Scroll</a>


<div class="social_block">
	<div class="wraper">
    	<p>Permanece conectado en las redes sociales</p>
      	<ul>
           	<li class="facebook"><a href="http://www.facebook.com/ITVillahermosa">Facebook</a></li>
            <li class="twitter"><a href="http://twitter.com/itvh">Twitter</a></li>
            <!--<li class="linkedin"><a href="#">LinkedIn</a></li>-->
            <li class="rss"><a href="http://www.youtube.com/user/ITVillahermosa1">YouTube</a></li>
            <li class="google"><a href="https://plus.google.com/101013671265577542882">Google+</a></li>
            <li class="rss"><a href="/rss.xml">RSS</a></li>
        </ul>
    </div><!--Cierra wraper del social block-->
</div><!-- /social block -->

<div class="footer">
	<center>	
    	<div style="max-width:110px;" class="gallery img bwWrapper">
        <a href="/images/logoitvhpie.png" rel="prettyPhoto[gallery2]">
            	<img src="/images/logoitvhpie.png" alt="ITVillahermosa" />
            </a>
        </div>
    </center>
</div>

<div class="copyright">
	<div class="wraper" style="text-align: center">
    	
        	Carretera Villahermosa - Frontera Km. 3.5 Ciudad Industrial<br />
            Villahermosa, Tabasco, Mexico. C.P. 86010<br />
            Teléfonos: 01(993) <!--353-02-59,--> 353-02-59 <a href="http://www.itvillahermosa.edu.mx/site/identidad.jsp?view=directoriotel">Extensiones</a><br />
            Instituto Tecnologico de Villahermosa - Copyright &copy; 2014 - Todos los Derechos Reservados<br />
            <a href="/">Inicio</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="#">Mapa del Sitio</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="">Politicas de Privacidad</a>
            <br /><br /><br />
        
    </div>
</div><!-- /copyright -->
<script type="text/javascript">
/*<![CDATA[*/
jQuery(function($) {
jQuery('a[rel="tooltip"]').tooltip();
jQuery('a[rel="popover"]').popover();
});
/*]]>*/
</script>
</body>
</html>