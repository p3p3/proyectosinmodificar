<?php
session_name("login");
session_start();

if ($_SESSION['autorizado']==true)
{

?>

<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" type="text/css" href="assets/fa4904c6/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="assets/fa4904c6/css/bootstrap-responsive.min.css" />
<link rel="stylesheet" type="text/css" href="assets/fa4904c6/css/bootstrap-yii.css" />
<link rel="stylesheet" type="text/css" href="assets/fa4904c6/css/jquery-ui-bootstrap.css" />
<link rel="stylesheet" type="text/css" href="css/pager.css" />
<script type="text/javascript" src="assets/7e42a305/jquery.min.js"></script>
<script type="text/javascript" src="assets/fa4904c6/js/bootstrap.bootbox.min.js"></script>
<script type="text/javascript" src="assets/fa4904c6/js/bootstrap.min.js"></script>
<title>ITVillahermosa</title>


<link rel="icon" type="image/png" href="/images/favicon.ico" />
<link rel="apple-touch-icon" href="themes/plantilla/images/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="themes/plantilla/images/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="themes/plantilla/images/apple-touch-icon-114x114.png">

<script language="javascript" src="JS/funciones3.js" type="text/javascript"></script>

<!--[if lt IE 9]>
	<script src="/themes/plantilla/scripts/html5.js"></script>
<![endif]-->

<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="themes/plantilla/stylesheets/style.css">
<link rel="stylesheet" href="themes/plantilla/stylesheets/responsive.css"> 
<link rel="stylesheet" href="themes/plantilla/stylesheets/jquery.onebyone.css">
<link rel="stylesheet" href="themes/plantilla/stylesheets/prettyPhoto.css">
<link rel="stylesheet" href="css/nivel.css">

<!--<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">-->
<!--<link rel="stylesheet" type="text/css" href="/css/bootstrap-responsive.min.css">-->

<!--<script src="/scripts/jquery.min.js"></script>--> 
<!--<script src="/js/bootstrap.js"></script>-->
<!--<script src="/js/bootstrap.min.js"></script>-->

<script src="themes/plantilla/scripts/jquery.prettyPhoto.js"></script>
<script src="themes/plantilla/scripts/jquery.blackandwhite.min.js"></script>
<script src="themes/plantilla/scripts/jquery.bxSlider.min.js"></script>
<script src="themes/plantilla/scripts/jquery.faq.js"></script> 
<script src="themes/plantilla/scripts/jquery.simpleFAQ-0.7.min.js"></script>  
<script src="themes/plantilla/scripts/jquery.onebyone.min.js"></script>              
<script src="themes/plantilla/scripts/jquery.touchwipe.min.js"></script> 
<script src="themes/plantilla/scripts/js_func.js"></script>

<!--<script src="/js/bootstrap-collapse.js"></script>-->




<script>
$(function(){
    $('#obo_slider').oneByOne({
		className: 'oneByOne1',	             
		easeType: 'random',
		slideShow: true
	});  

})
$(function(){
	$(".gallery a[rel^='prettyPhoto']").prettyPhoto({theme: 'dark_rounded'});
    
})

$(function(){
	/*$('.clients_slider').bxSlider({
		auto: false,
		controls : false,
		mode: 'fade',
		pager: true
	});*/	
	/*$('.recent_slider').bxSlider({
		auto: false,
		displaySlideQty: 1,
		moveSlideQty: 1,
    	speed : 1000
	});
	*/
	$('#faq').dltoggle();
	$("#open").click(function(event){
      $('#faq').dltoggle_show();
      return false;
	});
	$("#close").click(function(event){
      $('#faq').dltoggle_hide();
      return false;
	});
   
})

</script>
</head>
<body>

<!--start header-->


<section id="header">
<!-- Include the header bar -->
    <div class="wraper"> <!--abre envoltura -->
 
 <header class="encabezado">
 
	<div class="imagen1">
  		<img src="images/logosep.jpg" alt="SEP"/>
	</div>
       
  	<div class="titulo1">
    <p>Instituto Tecnológico de Villahermosa</p>
    </div>
    <div class="titulo2">
    <p>"Tierra, Tiempo, Trabajo y Tecnología"</p>
    </div>  
    
  </header>
</div> <!--cierra envoltura  del encabezado --><!-- /.container -->  
</section><!-- /#header --><!-- Require the header -->
<!-- Require the navigation -->
<div class="wraper"><!--inicia envoltura -->
	<section id="navigation-main">
    	<div class="navbar">
        	<div class="navbar-inner">
            	<a data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar">
                	<span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                
                <div class="nav-collapse" style="font-family:SoberanaTitularBold; font-size:0.96em">
                	<ul class="nav">
                    	
                        
                        <li class="dropdown">
                        	<a data-toggle="dropdown" class="dropdown-toggle" href="#">Registros <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                            	<li><a href="#"  onclick="llamarRegistroBien();">Bienes</a></li>
								<?php 
  
  
   if ($nivel==1||$nivel==2){ ?>
                                <li><a href="#" onclick="llamarRegistroTipobien();">Tipos de Bienes</a></li>
                                <li><a href="#"  onclick="llamarRegistroSala();">Salas</a></li>
                                <li><a href="#" onclick="llamarRegistroPrestador();">Prestadores</a></li>
                                <li><a href="#" onclick="llamarRegistroMarca();" >Marcas</a></li>
								<?php 
	}
	 
  ?>
  	<?php if($nivel==1) { ?>
								<li><a href="#" onclick="llamarRegistroUsuario();">Usuarios</a></li>
								 <?php } ?>
                            </ul>
                        </li>
                        
                        <li class="dropdown">
                        	<a data-toggle="dropdown" class="dropdown-toggle" href="#">Reportes <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                            	<li ><a href="PHP/imprimirBienes.php"  target="_blank">Todos los Bienes</a></li><br/>
  <li ><a href="#"  onclick="llamarReporteBienSala();">Mostrar Reporte de Bienes Por Sala</a></li>
                                	                                   </ul>
                                </li>
                            </ul>
                        </li>
                        
                                
                            
                    </ul>
                </div><!-- /.nav-collapse -->
            </div><!-- /#div nav bar inner -->
        </div><!-- /#div nav bar -->
    </section><!-- /#navigation-main -->
</div>  <!-- cierra la envoltura del menu -->
<div>

<div id="contenedor">

	<div id="cabecera">
	<input class="caja" name="nivel" id="nivel" type="text" value="<?php echo $_SESSION['nivel'] ;?>" hidden="hidden" disabled="disabled" size="25" maxlength="45" />
   
    
    
    
    <div id="sesion" align="right" class="nav">
	<?php
	if ($_SESSION['autorizado']==true)
{

echo "usuario: ".$_SESSION['usuario']."   |";
?>

<a href="PHP/cerrarSesion.php">Cerrar Sesion</a>
	<?php } ?>
    	
    </div>
    
	<div  class="navbar">
	<div class="navbar-inner">
    <div id="izquierdo" class="nav-collapse">
	
    
    </div>
	
	<div id="derecho">
    
    </div>
    </div>
	</div>
    <div id="cuerpo">
    	<div id="resultado" class="nav-collapse">
        <center>
       	
</center>
        </div>
    </div>
    
    
    
    <div id="pie">
    </div>

</div>

</div>
   
<div class="footer">
	<center>	
    	<div style="max-width:110px;" class="gallery img bwWrapper">
        <a href="images/logoitvhpie.png" rel="prettyPhoto[gallery2]">
            	<img src="images/logoitvhpie.png" alt="ITVillahermosa" />
            </a>
        </div>
    </center>
</div>

<div class="copyright">
	<div class="wraper" style="text-align: center">
    	
        	Carretera Villahermosa - Frontera Km. 3.5 Ciudad Industrial<br />
            Villahermosa, Tabasco, Mexico. C.P. 86010<br />
            Teléfonos: 01(993) <!--353-02-59,--> 353-02-59 <a href="http://www.itvillahermosa.edu.mx/site/identidad.jsp?view=directoriotel">Extensiones</a><br />
            Instituto Tecnologico de Villahermosa - Copyright &copy; 2014 - Todos los Derechos Reservados<br />
            <a href="/">Inicio</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="#">Mapa del Sitio</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="">Politicas de Privacidad</a>
            <br /><br /><br />
        
    </div>
</div><!-- /copyright -->
<script type="text/javascript">
/*<![CDATA[*/
jQuery(function($) {
jQuery('a[rel="tooltip"]').tooltip();
jQuery('a[rel="popover"]').popover();
});
/*]]>*/
</script>
</body>
</html>
<?php
}
else
{
	header("location:index.html");
}

?>